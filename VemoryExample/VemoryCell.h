//
//  VemoryCell.h
//  VemoryExample
//
//  Created by William Kluss on 11/22/14.
//  Copyright (c) 2014 Kluss Development. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VemoryCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *photoView;
@property (strong, nonatomic) IBOutlet UILabel *badgeLabel;

@end
