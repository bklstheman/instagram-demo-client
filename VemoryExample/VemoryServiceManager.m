//
//  VemoryServiceManager.m
//  VemoryExample
//
//  Created by William Kluss on 11/22/14.
//  Copyright (c) 2014 Kluss Development. All rights reserved.
//

#import "VemoryServiceManager.h"
#import "VemoryPhoto.h"
#import <AFNetworking.h>
#import <DLog/DLog.h>

@implementation VemoryServiceManager

+ (id)sharedInstance
{
    // structure used to test whether the block has completed or not
    static dispatch_once_t p = 0;
    
    // initialize sharedObject as nil (first call only)
    __strong static id _sharedObject = nil;
    
    // executes a block object once and only once for the lifetime of an application
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    
    // returns the same object each time
    return _sharedObject;
}

-(void)downloadPhotos {
    //Make the call to instragram to download photos
    
    AFHTTPRequestOperationManager *manger = [AFHTTPRequestOperationManager manager];
    [manger GET:@"https://api.instagram.com/v1/media/popular?client_id=[INSERT_CLIENT_ID]" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //SUCCESFULLY RESPONSE. NOW PARSE THE DATA
        DLog(@"Successful response. Begin parsing");
        NSDictionary *responseData = (NSDictionary *)responseObject[@"data"];
        DLog(@"%lu", (unsigned long)responseData.count);
        
        NSMutableArray *photos = [NSMutableArray new];
        for (NSDictionary *photoResponse in responseData) {
            if ([photoResponse[@"type"] isEqualToString:@"image"]) {
                VemoryPhoto *photo = [[VemoryPhoto alloc]initFromResponse:photoResponse];
                [photos addObject:photo];
            }
        }
        DLog(@"Number of photos %lu", photos.count);
        [self.delegate photosDidLoad:photos];

        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //Failed, send back an NSError delegate
        DLog(@"Failed to make the request");
        [self.delegate photosFailedToLoad:error];
    }];
}



@end
