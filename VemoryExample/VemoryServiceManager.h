//
//  VemoryServiceManager.h
//  VemoryExample
//
//  Created by William Kluss on 11/22/14.
//  Copyright (c) 2014 Kluss Development. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VermoryServiceManagerDelegate <NSObject>

/**
 *  Will return photos that have been downloaded
 *
 *  @param photos List of VemoryPhoto objects created from the response
 */
-(void)photosDidLoad:(NSArray *)photos;

/**
 *  Notify if an error occurred during downloading the photos
 *
 *  @param error NSError object returning back
 */
-(void)photosFailedToLoad:(NSError *)error;

@end

@interface VemoryServiceManager : NSObject

@property (weak, nonatomic) id<VermoryServiceManagerDelegate> delegate;

/**
 *  Go out to download the photos
 */
-(void)downloadPhotos;

+ (id)sharedInstance;


@end
