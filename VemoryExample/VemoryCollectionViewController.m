//
//  VemoryCollectionViewController.m
//  VemoryExample
//
//  Created by William Kluss on 11/22/14.
//  Copyright (c) 2014 Kluss Development. All rights reserved.
//

#import "VemoryCollectionViewController.h"
#import "VemoryPhoto.h"
#import "VemoryCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <MBProgressHUD/MBProgressHUD.h>

@interface VemoryCollectionViewController ()
@property (strong, nonatomic) VemoryServiceManager *vemoryManager;
@property (strong, nonatomic) NSArray *vemoryPhotos;
@property (strong, nonatomic) NSArray *vemoryPhotosByComments;
@property (strong, nonatomic) NSArray *vemoryPhotosByLikes;
@property (strong, nonatomic) IBOutlet UISegmentedControl *badgeControl;
@property (strong, nonatomic) NSSortDescriptor *commentSortDescriptor;
@property (strong, nonatomic) NSSortDescriptor *likeSortDescriptor;
@end

@implementation VemoryCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.vemoryManager = [VemoryServiceManager sharedInstance];
    self.vemoryManager.delegate = self;
    self.vemoryPhotos = @[];
    self.commentSortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"commentCount" ascending:NO];
    self.likeSortDescriptor = [[NSSortDescriptor alloc]initWithKey:@"likeCount" ascending:NO];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.vemoryManager downloadPhotos];
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.vemoryPhotos.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {

    NSUInteger cellWidth = self.collectionView.bounds.size.width/2;

    return CGSizeMake(cellWidth, cellWidth + 15);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    VemoryPhoto *photo = self.vemoryPhotos[indexPath.row];
    VemoryCell *cell = (VemoryCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"vemoryCell" forIndexPath:indexPath];
    
    if (self.badgeControl.selectedSegmentIndex == 0) {
        cell.badgeLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)photo.likeCount];
    } else {
        cell.badgeLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)photo.commentCount];
    }
    [cell.badgeLabel updateConstraintsIfNeeded];
    [cell.photoView sd_setImageWithURL:photo.standardURL];
    return cell;
}

-(void)photosDidLoad:(NSArray *)photos {    
    if (photos.count > 0) {
        self.vemoryPhotosByComments = [photos sortedArrayUsingDescriptors:@[self.commentSortDescriptor]];
        self.vemoryPhotosByLikes = [photos sortedArrayUsingDescriptors:@[self.likeSortDescriptor]];
        [self switchPhotoOrders:self.badgeControl];
    } else {
        //For some reason we got an empty list just display an error message
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unable to retrieve photos" message:@"Could not retrieve photos. Please try again." delegate:self cancelButtonTitle:@"Bummer" otherButtonTitles:nil, nil];
        [alert show];
    }
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

-(void)photosFailedToLoad:(NSError *)error {
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unable to retrieve photos" message:@"Could not retrieve photos. Please try again." delegate:self cancelButtonTitle:@"Bummer" otherButtonTitles:nil, nil];
    [alert show];
}

- (IBAction)switchPhotoOrders:(UISegmentedControl *)sender {
    if (self.badgeControl.selectedSegmentIndex == 0) {
        self.vemoryPhotos = self.vemoryPhotosByLikes;
    } else {
        self.vemoryPhotos = self.vemoryPhotosByComments;
    }
    
    [self.collectionView reloadData];
}

@end
