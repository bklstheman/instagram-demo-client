//
//  VemoryCollectionViewController.h
//  VemoryExample
//
//  Created by William Kluss on 11/22/14.
//  Copyright (c) 2014 Kluss Development. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VemoryServiceManager.h"
@interface VemoryCollectionViewController : UICollectionViewController<VermoryServiceManagerDelegate, UICollectionViewDelegateFlowLayout>

@end
