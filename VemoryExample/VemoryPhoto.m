//
//  VemoryPhoto.m
//  VemoryExample
//
//  Created by William Kluss on 11/22/14.
//  Copyright (c) 2014 Kluss Development. All rights reserved.
//

#import "VemoryPhoto.h"

@implementation VemoryPhoto

-(instancetype)initFromResponse:(NSDictionary *)response {
    self  = [super init];
    if (self) {
        
        _commentCount = [response[@"comments"][@"count"] integerValue];
        _likeCount = [response[@"likes"][@"count"] integerValue];
        _lowResURL = [NSURL URLWithString:response[@"images"][@"low_resolution"][@"url"]];
        _thumbnailURL = [NSURL URLWithString:response[@"images"][@"thumbnail"][@"url"]];
        _standardURL = [NSURL URLWithString:response[@"images"][@"standard_resolution"][@"url"]];
    }
    return self;
}

-(NSString *)description {
    return [NSString stringWithFormat:@"Comment Count: %lu\nLike Count: %lu\nLow Res URL: %@\nThumbnail URL: %@\nStandard URL: %@\n", self.commentCount, self.likeCount, self.lowResURL.absoluteString, self.thumbnailURL.absoluteString, self.standardURL.absoluteString];
}



@end
