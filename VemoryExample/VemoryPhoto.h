//
//  VemoryPhoto.h
//  VemoryExample
//
//  Created by William Kluss on 11/22/14.
//  Copyright (c) 2014 Kluss Development. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VemoryPhoto : NSObject

@property NSUInteger commentCount;
@property NSUInteger likeCount;
@property (strong, nonatomic) NSURL *lowResURL;
@property (strong, nonatomic) NSURL *thumbnailURL;
@property (strong, nonatomic) NSURL *standardURL;

/**
 *  Creates VemoryPhoto object from JSON response
 *
 *  @param response JSON response
 *
 *  @return VemoryPhoto Object
 */
-(instancetype)initFromResponse:(NSDictionary *)response;


@end
